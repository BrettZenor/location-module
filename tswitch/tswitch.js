(function($){
	$.fn.tswitch = function (options) {
		var defaults = {
			loadid: "tswitch-load",
			enabled: true,
			width: 60,
			labels: [
				"false",
				"true"
			]
		};
		var data = $.extend({}, defaults, options);
		$("select#"+data.loadid).after("<div onclick='"+data.func+"' id='tswitch'></div>").remove();
		var e = $("div#tswitch");
		var label = "<span class='on'>"+data.labels[1]+"</span><span class='off'>"+data.labels[0]+"</span>";
		if (data.enabled) {
			e.addClass("enabled");
		}
		
		e.width(data.width).append(label+"<div id='cover'></div>");
		console.log(data);
		return this;
	};
	$.fn.tswitch_toggle = function () {
		$("div#tswitch").toggleClass("enabled");
		return this;
	}
}(jQuery));