(function($){
	$.fn.mapper = function(Latitude, Longitude, Marker, Message, ZoomLevel) {
		$("#search").keyup(function(event){
			if(event.keyCode == 13){
				$("#search-submit").click();
			}
		});
		
		directionsService = new google.maps.DirectionsService();
		
		var map,centered;
		var stylers = [
		  {
			"featureType": "road",
			"elementType": "geometry.fill",
			"stylers": [
			  { "saturation": -100 },
			  { "lightness": 100 }
			]
		  },
		  {
			"featureType": "road",
			"elementType": "geometry.stroke",
			"stylers": [
			  { "color": "#ff0000" }
			]
		  },
		  {
			"featureType": "road.local",
			"elementType": "geometry.stroke",
			"stylers": [
			  { "color": "#0080ff" }
			]
		  },
		  {
			"featureType": "poi",
			"elementType": "labels",
			"stylers": [
			  { "visibility": "off" }
			]
		  },
		  {
			"featureType": "road",
			"elementType": "labels",
			"stylers": [
			  { "visibility": "on" },
			  { "weight": 0.1 },
			  { "saturation": -100 },
			  { "lightness": 56 }
			]
		  },
		  {
			"featureType": "road.arterial",
			"elementType": "labels",
			"stylers": [
			  { "lightness": -100 }
			]
		  }
		];
		function initialize() {
		  var rendererOptions = {
            draggable: true,
            suppressMarkers: false,
            polylineOptions: {
              strokeColor: "#FF0000",
              strokeOpacity: 0.8,
              strokeWeight: 5
            }
          };
		  directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions);
		  
		  /**
		   * Define varaibles and remove displayed image field
		   */
		  centered = new google.maps.LatLng(Latitude, Longitude); //Latitude and Longitude stored in centered variable
		  
		  /*var field = $('.field-name-field-marker .field-item img');
		  Marker = field? field.attr('src'): ''; //Create a marker with full path to image
		  $('div.field').remove('.field-type-text').remove('.field-type-image'); //Remove fields from display*/
		  
		  /**
		   * Define map options
		   */
		  var mapoptions = {
			zoom: ZoomLevel,
			center: centered,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			mapTypeControl: true,
			mapTypeControlOptions: {
			  style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
			  position: google.maps.ControlPosition.TOP_RIGHT
			},
			zoomControl: true,
			zoomControlOptions: {
			  style: google.maps.ZoomControlStyle.NORMAL,
			  position: google.maps.ControlPosition.TOP_LEFT
			},
			panControl: false,
			streetViewControl: false,
			scaleControl: true,
			scrollwheel: true,
			styles: stylers
		  }
		  
		  /**
		   * Define map in div with mapoptions
		   */
		  map = new google.maps.Map(document.getElementById("map-canvas"),mapoptions);
		  
		  directionsDisplay.setMap(map);
		  directionsDisplay.setPanel(document.getElementById('directions-panel'));
		  
		  /**
		   * Marker script below
		   */
		  var marker = new google.maps.Marker({
			position: centered,
			map: map,
			icon: Marker,
			title:"Chavela's NYC",
			animation: google.maps.Animation.DROP
		  });
		  //var message = '<p>Test text</p>';
		  var infowindow = new google.maps.InfoWindow({
			content: Message
		  });
		  google.maps.event.addListener(marker, 'click', function () {
			infowindow.open(map,marker);
		  });
		}
		initialize();
		
		var last = false;
		function location_change_travelmode(methods, index) {
			if(last!=false){
				if (last!=index) {
					$('.mode-selector.active').removeClass('active');
					$('.mode-selector').eq(index).addClass('active');
					selectedMode = methods[index];
					console.log("Travel mode: "+selectedMode);
				}
			} else {
				$('.mode-selector.active').removeClass('active');
				$('.mode-selector').eq(index).addClass('active');
				selectedMode = methods[index];
				console.log("Travel mode: "+selectedMode);
			}
			last = index;
		}
		/**
		 * jQuery
		 */
		//$(document).ready(function(){
			modes = ["TRANSIT","DRIVING","WALKING","BICYCLING"];
			location_change_travelmode(modes, 0);
			$('.mode-selector').click(function(e){
				index = $(this).index();
				e.preventDefault();
				location_change_travelmode(modes, index);
				$.fn.location_calculate_route();
			});
		//});
		return this;
	}
	
	$.fn.location_calculate_route = function() {
		var defaults = {
			destination: new google.maps.LatLng(Latitude, Longitude),
			travelMode: google.maps.TravelMode[selectedMode],
			provideRouteAlternatives: true
		}
		function location_calculate_start(check) {
			if (check==false) {
				var startvalue = $('#search').attr('value');
				if (startvalue!=""){
					return {origin: startvalue};
				}
			} else {
				return {origin: new google.maps.LatLng(currentLatitude, currentLongitude)};
			}
		}
		var request = $.extend({}, defaults, location_calculate_start(check));
		/*	origin: start,
			destination: end,
			travelMode: google.maps.TravelMode[selectedMode]*/
		if (request.origin){
			directionsService.route(request, function(response, status) {
				if (status == google.maps.DirectionsStatus.OK) {
					directionsDisplay.setDirections(response);
				} else {
					alert("Could not find directions for this travel method");
				}
			});
		}
		return this;
	}
}(jQuery));