//determine if the handset has client side geo location capabilities
var check = false;
function init_geo_location(){
	if(check==false) {
		if(geo_position_js.init()){
		   geo_position_js.getCurrentPosition(geo_success_callback,geo_error_callback);
		} else {
		   alert("Functionality not available");
		}
	} else {
		jQuery.fn.tswitch_toggle();
		jQuery('#search,#search-submit').removeAttr('disabled');
		check = false;
	}
}
function geo_success_callback(p){
	check = true;
	jQuery('#search,#search-submit').attr('disabled','disabled');
	currentLatitude = p.coords.latitude.toFixed(10);
	currentLongitude = p.coords.longitude.toFixed(10);
	console.log('Current location: '+currentLatitude+', '+currentLongitude);
	jQuery.fn.tswitch_toggle();
	jQuery.fn.location_calculate_route();
}
function geo_error_callback(p){
	console.log('error code: '+p.code);
	alert('Error: Check if GPS is enabled and try again.');
}